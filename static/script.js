function getHTML(src, title, imdb_id) {
  const b = document.createElement("button");
  b.addEventListener("click", async () => {
    document.querySelector("input[type=search]").value = "";
    const res = await fetch("/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        imdb_id,
      }),
    });
    const text = await res.text();
    if (text) {
      location.assign(text);
      return;
    }
    location.reload();
  });
  const i = document.createElement("img");
  i.src = src;
  const h = document.createElement("h2");
  const t = document.createTextNode(title);
  h.appendChild(t);
  b.appendChild(i);
  b.appendChild(h);
  return b;
}

const updateSearchResut = async () => {
  const search = document.querySelector("input[type=search]").value;
  const cont = document.querySelector("#results-list");
  cont.innerHTML = "";
  document.querySelector("#search").classList.remove("bottom");

  if (search === "") {
    return;
  }

  /** @type {{ title: string, imdb_id: number, small_cover_image: string }[]} */
  const results = await fetch("/list", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query_term: search,
    }),
  }).then((res) => res.json());

  results
    .map((m) => getHTML(m.small_cover_image, m.title, m.imdb_id))
    .forEach((c) => {
      cont.appendChild(c);
    });

  if (results.length === 0) {
    const d = document.createElement("div");
    d.textContent = "No results found";
    cont.appendChild(d);
  }

  document.querySelector("#search").classList.add("bottom");
};

let searchTimeout = null;

document
  .querySelector("input[type=search]")
  .addEventListener("input", async (e) => {
    clearTimeout(searchTimeout);
    searchTimeout = setTimeout(updateSearchResut, 500);
  });
