use itertools::Itertools;
use qbit_rs::Qbit;
use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Serialize)]
pub enum Status {
    Unstarted,
    Ongoing { done: u64, total: u64 },
    Done,
}

pub struct Metainfo {
    pub size: u64,
    pub title: String,
    pub cover_image: url::Url,
    pub quality: String,
}

pub struct State {
    pub torrent_uri: url::Url,
    pub hash: String,
    pub imdb_id: u64,
    pub metainfo: Metainfo,
}

impl State {
    pub async fn status(&self, client: &Qbit) -> Status {
        let Ok(torrent) = client.get_torrent_properties(&self.hash).await else {
            return Status::Unstarted;
        };
        match (torrent.pieces_num, torrent.pieces_have) {
            (Some(a), Some(b)) if a == b => Status::Done,
            (Some(a), Some(b)) => Status::Ongoing {
                done: b as u64,
                total: a as u64,
            },
            _ => Status::Unstarted,
        }
    }

    pub async fn status_string(&self, client: &Qbit) -> String {
        match self.status(client).await {
            Status::Unstarted => "Unstarted".to_owned(),
            Status::Ongoing { done, total } => format!("{}%", 100 * done / total),
            Status::Done => "Completed".to_owned(),
        }
    }

    pub fn title(&self) -> String {
        self.metainfo.title.clone()
    }
}

#[derive(Deserialize)]
struct Torrent {
    quality: String,
    size_bytes: u64,
    url: url::Url,
    hash: String,
}

#[derive(Deserialize)]
struct MovieDetails {
    title: String,
    imdb_code: String,
    small_cover_image: url::Url,
    medium_cover_image: url::Url,
    torrents: Vec<Torrent>,
}

#[derive(Deserialize)]
struct DetailsRequestData {
    movie: MovieDetails,
}

#[derive(Deserialize, Default)]
struct ListRequestData {
    movies: Vec<MovieDetails>,
}

#[derive(Deserialize)]
struct DetailsRequestResult {
    data: DetailsRequestData,
}

#[derive(Deserialize, Default)]
struct ListRequestResult {
    data: ListRequestData,
}

#[derive(Debug, Serialize)]
pub struct OnlineMetainfo {
    pub title: String,
    pub imdb_id: u64,
    pub size: u64,
    pub quality: String,
    pub small_cover_image: url::Url,
    pub cover_image: url::Url,
    pub torrent_uri: url::Url,
    pub hash: String,
}

pub async fn imdbs(query_term: String) -> Vec<OnlineMetainfo> {
    let mut url = url::Url::parse(std::option_env!("SOURCE").unwrap()).expect("url parsing failed");
    url.path_segments_mut().unwrap().push("list_movies.json");
    url.query_pairs_mut()
        .append_pair("query_term", &query_term)
        .append_pair("sort_by", "download_count")
        .append_pair("order_by", "desc");

    reqwest::get(url)
        .await
        .unwrap()
        .json::<ListRequestResult>()
        .await
        .unwrap_or_default()
        .data
        .movies
        .into_iter()
        .map(collect_metainfo)
        .take(5)
        .collect()
}

fn collect_metainfo(movie: MovieDetails) -> OnlineMetainfo {
    const ORDER: [&str; 2] = ["1080p", "720p"];

    let best_torrent = movie
        .torrents
        .into_iter()
        .sorted_by_key(|a| {
            ORDER
                .into_iter()
                .position(|s| a.quality == s)
                .unwrap_or(ORDER.len() + 1)
        })
        .next()
        .unwrap();

    OnlineMetainfo {
        title: movie.title,
        imdb_id: movie.imdb_code.strip_prefix("tt").unwrap().parse().unwrap(),
        quality: best_torrent.quality,
        small_cover_image: movie.small_cover_image,
        cover_image: movie.medium_cover_image,
        size: best_torrent.size_bytes,
        torrent_uri: best_torrent.url,
        hash: best_torrent.hash,
    }
}

pub async fn metainfo(imdb_id: u64) -> OnlineMetainfo {
    let movie = reqwest::get(format!(
        "{}/movie_details.json?imdb_id={:07}",
        std::option_env!("SOURCE").unwrap(),
        imdb_id
    ))
    .await
    .unwrap()
    .json::<DetailsRequestResult>()
    .await
    .unwrap()
    .data
    .movie;

    collect_metainfo(movie)
}
