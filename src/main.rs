mod torrent;

use std::fs::OpenOptions;
use std::{fs, vec};

use actix_web::dev::ServiceRequest;
use actix_web::{get, post, web, App, HttpServer, Responder};
use actix_web_httpauth::extractors::basic::{self, BasicAuth};
use actix_web_httpauth::extractors::AuthenticationError;
use itertools::Itertools;
use maud::html;
use qbit_rs::model::{AddTorrentArg, Credential, GetTorrentListArg};
use qbit_rs::Qbit;
use serde::Deserialize;
use torrent::metainfo;

struct AppState {
    torrents: boxcar::Vec<torrent::State>,
    client: Qbit,
}

#[derive(Deserialize)]
struct PageInfo {
    #[serde(default)]
    page: usize,
}

#[get("/")]
async fn index(data: web::Data<AppState>, info: web::Query<PageInfo>) -> impl Responder {
    let client = &data.client;
    html! {
        (maud::DOCTYPE)
        html {
            head {
                meta charset="utf-8";
                title { "Arthur Streaming Service" }
                link rel="stylesheet" type="text/css" href="/static/stylesheet.css";
                meta name="viewport" content="width=device-width, initial-scale=1.0";
            }
            body {
                h1 {
                    "ASS – Arthur Streaming Service"
                }

                div id="search" {
                    input type="search" incremental name="query_term" id="query_term" placeholder="Search";

                    div id="results" {
                        div id="results-list" {

                        }
                    }
                }

                ul {
                    @for (_, torrent) in data.torrents.iter().collect_vec().into_iter().rev().skip(50*info.page).take(50) {
                        li {
                            img src=[Some(torrent.metainfo.cover_image.clone())];

                            span {
                                (torrent.metainfo.quality)
                            }

                            div {
                                h2 {
                                    (torrent.title())
                                    @match torrent.status_string(client).await {
                                        x if x == "Completed" => "",
                                        x => " (" (x) ")"
                                    };
                                }

                                @let files = client.get_torrent_contents(&torrent.hash, None).await.unwrap_or_default();

                                @let watch = files.iter().filter(|f| f.name.ends_with(".mp4")).collect_vec();

                                @let subtitles = files.iter().filter(|f| f.name.ends_with(".srt") || f.name.ends_with(".sub")).collect_vec();

                                ol {
                                    @for movie_file in watch {
                                        @if movie_file.progress == 1. {
                                            li {
                                                a href=[Some(format!("/movies/{}", movie_file.name))] download {
                                                    img src="/static/download.svg";
                                                }
                                            }
                                        }

                                        li {
                                            a href=[Some(format!("/movies/{}", movie_file.name))] {
                                                img src="/static/play.svg";
                                            }
                                        }
                                    }

                                    @for file in subtitles {
                                        li {
                                            a href=[Some(format!("/movies/{}", file.name))] {
                                                img src="/static/cc.svg";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                script src="/static/script.js" {}

                @let page_count = data.torrents.count().div_ceil(50).max(1);

                h3 {
                    "Page: " (info.page)
                }

                div id="pages" {
                    @if info.page != 0 {
                        a href="/" {
                            "First Page"
                        }
                    }

                    @if info.page > 1 {
                        a href=[Some(format!("/?page={}", info.page - 1))] {
                            "Previous Page"
                        }
                    }

                    @if info.page + 1 < page_count - 1 {
                        a href=[Some(format!("/?page={}", info.page + 1))] {
                            "Next Page"
                        }
                    }

                    @if info.page != page_count - 1 {
                        a href=[Some(format!("/?page={}", page_count - 1))] {
                            "Last Page"
                        }
                    }
                }

            }
        }
    }
}

#[derive(Deserialize)]
struct ListData {
    query_term: String,
}

#[post("/list")]
async fn list(_data: web::Data<AppState>, form: web::Json<ListData>) -> impl Responder {
    web::Json(torrent::imdbs(form.query_term.clone()).await)
}

#[derive(Deserialize)]
struct FormData {
    imdb_id: u64,
}

#[post("/")]
async fn push(data: web::Data<AppState>, form: web::Json<FormData>) -> impl Responder {
    let imdb_id: u64 = form.imdb_id;

    if let Some((_, torrent)) = data.torrents.iter().find(|(_, t)| t.imdb_id == imdb_id) {
        let files = data
            .client
            .get_torrent_contents(torrent.hash.clone(), None)
            .await
            .unwrap_or_default();

        let watch = files
            .iter()
            .filter(|f| f.name.ends_with(".mp4"))
            .collect_vec();

        if let Some(movie) = watch.first() {
            return format!("/movies/{}", movie.name);
        }

        return String::new();
    }

    let m = metainfo(imdb_id).await;

    data.torrents.push(torrent::State {
        torrent_uri: m.torrent_uri.clone(),
        hash: m.hash,
        imdb_id,
        metainfo: torrent::Metainfo {
            size: m.size,
            cover_image: m.cover_image,
            title: m.title,
            quality: m.quality,
        },
    });

    save_disk(imdb_id);

    let client = &data.client;
    add_torrent(m.torrent_uri.clone(), client).await;

    String::new()
}

async fn add_torrent(torrent_uri: url::Url, client: &Qbit) {
    client
        .add_torrent(AddTorrentArg {
            source: qbit_rs::model::TorrentSource::Urls {
                urls: vec![torrent_uri].into(),
            },
            sequential_download: Some("true".to_owned()),
            savepath: Some(
                std::path::Path::new("torrents")
                    .canonicalize()
                    .unwrap()
                    .to_str()
                    .unwrap()
                    .to_owned(),
            ),
            ..Default::default()
        })
        .await
        .unwrap();
}

async fn reconcile(movies: Vec<u64>, client: &mut Qbit) -> Vec<torrent::State> {
    let tasks = movies
        .into_iter()
        .map(|imdb_id| actix_web::rt::spawn(async move { metainfo(imdb_id).await }))
        .collect_vec();

    let metainfos = futures::future::join_all(tasks)
        .await
        .into_iter()
        .flatten()
        .collect_vec();

    let existing_torrents = client
        .get_torrent_list(GetTorrentListArg {
            ..Default::default()
        })
        .await
        .unwrap();

    let mut states = Vec::new();

    for m in metainfos {
        let torrent_uri = m.torrent_uri.clone();

        if !existing_torrents
            .iter()
            .any(|t| matches!(&t.hash, Some(h) if h.to_uppercase() == m.hash.to_uppercase()))
        {
            add_torrent(torrent_uri, client).await;
        };

        states.push(torrent::State {
            torrent_uri: m.torrent_uri,
            imdb_id: m.imdb_id,
            hash: m.hash,
            metainfo: torrent::Metainfo {
                size: m.size,
                cover_image: m.cover_image,
                title: m.title,
                quality: m.quality,
            },
        })
    }

    states
}

fn load_disk() -> Vec<u64> {
    use std::io::prelude::*;

    let content = fs::read_to_string("movies.txt").unwrap_or_default();
    let ids = content
        .lines()
        .map(|a| a.trim())
        .filter(|a| !a.is_empty())
        .map(|a| a.parse().unwrap())
        .unique()
        .collect_vec();

    let mut file = OpenOptions::new()
        .create(true)
        .write(true)
        .truncate(true)
        .open("movies.txt")
        .unwrap();

    for imdb_id in ids.iter() {
        writeln!(file, "{:07}", imdb_id).expect("must be able to write to file");
    }

    ids
}

fn save_disk(imdb_id: u64) {
    use std::io::prelude::*;

    let mut file = OpenOptions::new().append(true).open("movies.txt").unwrap();

    if let Err(e) = writeln!(file, "{:07}", imdb_id) {
        eprintln!("Couldn't write to file: {}", e);
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let _ = fs::create_dir("torrents");

    let mut client = Qbit::builder()
        .endpoint("http://127.0.0.1:9090")
        .credential(Credential::dummy())
        .build();

    let torrents = reconcile(load_disk(), &mut client).await;

    let state = web::Data::new(AppState {
        torrents: boxcar::Vec::from_iter(torrents.into_iter()),
        client,
    });

    async fn validator(
        req: ServiceRequest,
        credentials: BasicAuth,
    ) -> Result<ServiceRequest, (actix_web::Error, ServiceRequest)> {
        if credentials.password() == option_env!("PASSWORD") {
            Ok(req)
        } else {
            let config = req.app_data::<basic::Config>().cloned().unwrap_or_default();

            Err((AuthenticationError::from(config).into(), req))
        }
    }

    HttpServer::new(move || {
        App::new()
            .wrap(actix_web_httpauth::middleware::HttpAuthentication::basic(
                validator,
            ))
            .app_data(state.clone())
            .service(index)
            .service(list)
            .service(push)
            .service(actix_files::Files::new("/static", "./static").show_files_listing())
            .service(actix_files::Files::new("/movies", "./torrents").show_files_listing())
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
